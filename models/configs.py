import json


class ModelConfig(object):
    """default config for NER model

    """
    def __init__(self):
        self.learning_rate = 0.001
        self.exponential_decay = False
        self.decay_rate = 0.99
        self.decay_steps = 10
        self.batch_size = 128
        self.epochs = 1000
        self.output_n_epochs = 10
        self.l2_norm = 0

        self.min_length = 2
        self.text_line_wrap = True
        self.n_tags = 21  # 5 entity and 1 None

        self.use_crf = True

        self.vocab_size = 50000
        self.embedding_matrix_path = None
        self.embedding_matrix = None
        self.embedding_size = 128
        self.train_embedding = True  # if False, embedding matrix need be pre-trained

        # lstm config
        self.rnn_type = 'lstm'  # optional: lstm, gru
        self.rnn_size = 128
        self.rnn_drop = 0  # keep_prob = 1 - rnn_drop

        # entity dependency config
        self.n_step = 3

        # cnn config
        self.filter_size = 3  # filter size is [filter_size * embedding_size]
        self.filter_num = 128

        self.save_path = "./model_save/"
        self.model_name = "NERModel"

        self.model_type = 'rnn'  # optional [rnn, cnn, entity]

        self.save_n_times = 5

    @property
    def buffer_size(self):
        return self.batch_size * 10

    @property
    def save_n_steps(self):
        return int(self.epochs / self.save_n_times)

    def __eq__(self, other):
        if isinstance(other, ModelConfig):
            if len(self.__dict__) != len(other.__dict__):
                return False
            for f, s in zip(self.__dict__.items(), other.__dict__.items()):
                if f != s:
                    return False
            return True
        return NotImplemented


class W2VConfig(object):
    def __init__(self):
        self.vocab_size = 50000
        self.embedding_size = 128
        # 0意味着未知的词放在词典的第一位
        # 1 意味着0用于补齐句子的长度，1是未知的词
        self.unknown_word = 0  # 0, 1
        self.min_count = 5
        self.window_size = 5

        self.save_root = '../resources/w2v/ner_corpus'
        self.word2id_name = 'word2id.cpkt'
        self.embedding_matrix_name = 'embedding_matrix.cpkt'

    def __eq__(self, other):
        if isinstance(other, W2VConfig):
            if len(self.__dict__) != len(other.__dict__):
                return False
            for f, s in zip(self.__dict__.items(), other.__dict__.items()):
                if f != s:
                    return False
            return True
        return NotImplemented


def load_model_config(config_file):
    with open(config_file, 'r', encoding='utf-8') as f:
        con = json.load(f)
    c = ModelConfig()
    for k, v in con.items():
        setattr(c, k, v)
    return c


def load_w2v_config(config_file):
    with open(config_file, 'r', encoding='utf-8') as f:
        con = json.load(f)
    c = W2VConfig()
    for k, v in con.items():
        setattr(c, k, v)
    return c


def save_config(config, config_file):
    with open(config_file, 'w', encoding='utf-8') as f:
        json.dump(config.__dict__, f, indent=2, separators=(',', ': '), default=lambda o: o.__dict__)
