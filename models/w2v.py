import os
import argparse
import _pickle
import json

import gensim
import numpy as np

from .configs import W2VConfig


__all__ = ['read_all_ehr',
           'read_ner_corpus',
           'train_save_w2v',
           'load_w2v']


def read_all_ehr(root_path='../resources/all_ehr'):
    sentences = []
    filenames = os.listdir(root_path)
    for filename in filenames:
        with open(os.path.join(root_path, filename), 'r', encoding='utf-8') as f:
            sentence = list(f.readline())
            sentences.append(sentence)

    sentences.extend(read_ner_corpus('../resources/corpus/result.txt'))
    sentences.extend(read_ner_corpus('../resources/corpus/train_data'))
    sentences.extend(read_ner_corpus('../resources/corpus/test_data'))
    return sentences


def read_ner_corpus(file_path='../resources/corpus/train_data'):
    sentences = []
    with open(file_path, 'r', encoding='utf-8') as f:
        sentence = []
        for line in f:
            if line == '\n':
                sentences.append(sentence)
                sentence = []
            else:
                word, _ = line.split('\t')
                sentence.append(word)
    return sentences


def train_save_w2v(sentences, w2v_config=None):
    """

    :param sentences: sentences to train w2v model. 2-dimension list [['今', '日', ...], ['查', '房', ...], ...]
    :param W2VConfig w2v_config: config of word embedding
    :return:
    """
    model = gensim.models.Word2Vec(sentences,
                                   size=w2v_config.embedding_size,
                                   min_count=w2v_config.min_count,
                                   window=w2v_config.window_size,
                                   max_vocab_size=w2v_config.vocab_size)
    wv = model.wv
    id2word = wv.index2word
    word2id = {w: i for i, w in enumerate(id2word, w2v_config.unknown_word + 1)}
    vocab_size = len(id2word)
    w2v_config.vocab_size = vocab_size

    if w2v_config.unknown_word == 0:
        word2id['UNK'] = 0
        embedding_matrix = np.zeros([vocab_size+1, w2v_config.embedding_size])
    elif w2v_config.unknown_word == 1:
        word2id['UNK'] = 1
        embedding_matrix = np.zeros([vocab_size+2, w2v_config.embedding_size])
    else:
        raise ValueError('value of unknown word: {} is not supported'.format(w2v_config.unknown_word))

    for w, i in word2id.items():
        if w == 'UNK':
            continue
        embedding_matrix[i] = wv[w]

    if not os.path.exists(w2v_config.save_root):
        os.mkdir(w2v_config.save_root)

    with open(os.path.join(w2v_config.save_root, 'word2id.cpkt'), 'wb') as wf:
        _pickle.dump(word2id, wf)

    embedding_matrix.dump(os.path.join(w2v_config.save_root, 'embedding_matrix.cpkt'))

    with open(os.path.join(w2v_config.save_root, 'w2v_config.json'), 'w') as f:
        w2v_config.save_root = './'
        json.dump(w2v_config.__dict__, f, indent=2, separators=(',', ':'))

    return word2id, embedding_matrix


def load_w2v(w2v_path):
    with open(os.path.join(w2v_path, 'word2id.cpkt'), 'rb') as rf:
        word2id = _pickle.load(rf)

    embedding_matrix = np.load(os.path.join(w2v_path, 'embedding_matrix.cpkt'))

    return word2id, embedding_matrix


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('--config_file', type=str)
    parser.add_argument('--data_source', type=int, choices=[0, 1], default=0,
                        help="if 0, use ner corpus, if 1, use all ehr corpus")

    args = parser.parse_args()

    config = W2VConfig()

    if args.config_file is not None:
        with open(args.config_file, 'r', encoding='utf-8') as f:
            con = json.load(f)

        for k, v in con.items():
            setattr(config, k, v)

    if args.data_source == 0:
        sentences = read_ner_corpus()
    elif args.data_source == 1:
        sentences = read_all_ehr()
    else:
        raise ValueError('unknown data source')

    train_save_w2v(sentences, config)


if __name__ == "__main__":
    main()
