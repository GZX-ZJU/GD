import tensorflow as tf

from .rnn import RNNModel


class CNNModel(RNNModel):
    def build(self):
        with tf.variable_scope(self._config.model_name):
            self._placeholder()
            self._build_set()
            self._embedding_layer()
            self._cnn_layer()
            self._pred_layer()
            self._loss_def()
            self._train_def()
            self._init_session()

    def _cnn_layer(self):
        self._embedding_inputs = tf.pad(self._embedding_inputs, [[0, 0], [1, 1], [0, 0]])
        self._embedding_inputs = tf.expand_dims(self._embedding_inputs, -1)
        self._hidden = tf.layers.conv2d(self._embedding_inputs,
                                        self._config.filter_num,
                                        (self._config.filter_size, self._config.embedding_size))
        output = tf.reshape(self._hidden, [-1, self._config.embedding_size])
        pred = tf.contrib.layers.fully_connected(output, self._config.n_tags, activation_fn=None)
        current_max_length = tf.shape(self._input_batch)[1]
        self._logits = tf.reshape(pred, [-1, current_max_length, self._config.n_tags], 'logits')


class CNNRNNModel(RNNModel):
    def build(self):
        with tf.variable_scope(self._config.model_name):
            self._placeholder()
            self._build_set()
            self._embedding_layer()
            self._cnn_layer()
            self._rnn_layer()
            self._pred_layer()
            self._loss_def()
            self._train_def()
            self._init_session()

    def _cnn_layer(self):
        self._embedding_inputs = tf.pad(self._embedding_inputs, [[0, 0], [1, 1], [0, 0]])
        self._embedding_inputs = tf.expand_dims(self._embedding_inputs, -1)
        self._rnn_inputs = tf.layers.conv2d(self._embedding_inputs,
                                            self._config.filter_num,
                                            (self._config.filter_size, self._config.embedding_size))
