
from .base import *
from .rnn import *
from .cnn import *
from .configs import *
from .w2v import *


def make_model(config):
    """

    :param ModelConfig config:
    :return:
    """
    if config.model_type == 'rnn':
        return RNNModel(config)
    elif config.model_type == 'entity':
        return EntityDependencyRNNModel(config)
    elif config.model_type == 'cnn':
        return CNNModel(config)
    else:
        raise NotImplementedError

