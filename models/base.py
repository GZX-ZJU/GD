import abc

from .configs import ModelConfig


class NERModel(metaclass=abc.ABCMeta):
    def __init__(self, config):
        """Base Model class for Named Entity Recognition

        :param ModelConfig config: 命名实体识别模型的参数配置类
        """
        self._config = config
        self.build()

    @abc.abstractmethod
    def build(self):
        pass

    @abc.abstractmethod
    def fit(self, data_set, retrain=False):
        pass

    @abc.abstractmethod
    def predict(self, data_set):
        pass

    @abc.abstractmethod
    def save(self, path, step=None):
        pass

    @abc.abstractmethod
    def restore(self, path):
        pass
