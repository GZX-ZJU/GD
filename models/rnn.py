import os

import tensorflow as tf

from .base import NERModel
from data import DataSet


__all__ = ['RNNModel',
           'EntityDependencyRNNModel']


class RNNModel(NERModel):
    def build(self):
        with tf.variable_scope(self._config.model_name):
            self._placeholder()
            self._build_set()
            self._embedding_layer()
            self._rnn_layer()
            self._pred_layer()
            self._loss_def()
            self._train_def()
            self._init_session()

    def _placeholder(self):
        self._inputs = tf.placeholder(tf.int32, [None, None], "inputs")
        self._lengths = tf.placeholder(tf.int32, [None], 'lengths')
        self._targets = tf.placeholder(tf.int32, [None, None], "targets")
        self._drop_rate = tf.placeholder_with_default(self._config.rnn_drop, [], 'rnn_drop')
        self._global_step = tf.Variable(0, dtype=tf.int32, trainable=False, name='global_step')
        if self._config.exponential_decay:
            self._learning_rate = tf.train.exponential_decay(self._config.learning_rate,
                                                             self._global_step,
                                                             self._config.decay_steps,
                                                             self._config.decay_rate)
        else:
            self._learning_rate = tf.constant(self._config.learning_rate)

    def _build_set(self):
        self._data_set = tf.data.Dataset.from_tensor_slices((self._inputs, self._lengths, self._targets))
        self._data_set = self._data_set.repeat().shuffle(self._config.buffer_size).batch(self._config.batch_size)

        self._iter = self._data_set.make_initializable_iterator()

        self._input_batch, self._length_batch, self._target_batch = self._iter.get_next()

    def _embedding_layer(self):
        with tf.device('/cpu:0'):
            if self._config.embedding_matrix is not None:
                self._embedding = tf.Variable(self._config.embedding_matrix,
                                              dtype=tf.float32,
                                              trainable=self._config.train_embedding,
                                              name='embedding')
            else:
                self._embedding = tf.get_variable('embedding',
                                                  [self._config.vocab_size, self._config.embedding_size],
                                                  trainable=self._config.train_embedding)
            self._embedding_inputs = tf.nn.embedding_lookup(self._embedding,
                                                            self._input_batch,
                                                            name="embedding_layer")

    def _rnn_layer(self):
        if self._config.rnn_type == 'lstm':
            self._cell_fw = tf.contrib.rnn.BasicLSTMCell(self._config.rnn_size)
            self._cell_bw = tf.contrib.rnn.BasicLSTMCell(self._config.rnn_size)
        elif self._config.rnn_type == 'gru':
            self._cell_fw = tf.contrib.rnn.GRUCell(self._config.rnn_size)
            self._cell_bw = tf.contrib.rnn.GRUCell(self._config.rnn_size)

        if self._config.rnn_drop:
            self._cell_fw = tf.contrib.rnn.DropoutWrapper(self._cell_fw,
                                                          output_keep_prob=1-self._drop_rate)
            self._cell_bw = tf.contrib.rnn.DropoutWrapper(self._cell_bw,
                                                          output_keep_prob=1-self._drop_rate)

        self._state_fw = self._cell_fw.zero_state(tf.shape(self._input_batch)[0], tf.float32)
        self._state_bw = self._cell_bw.zero_state(tf.shape(self._input_batch)[0], tf.float32)

        self._hidden, _ = tf.nn.bidirectional_dynamic_rnn(self._cell_fw,
                                                          self._cell_bw,
                                                          self._embedding_inputs,
                                                          sequence_length=self._length_batch,
                                                          initial_state_fw=self._state_fw,
                                                          initial_state_bw=self._state_bw)
        self._hidden_concat = tf.concat(self._hidden, -1)
        output = tf.reshape(self._hidden_concat, [-1, 2 * self._config.rnn_size])
        pred = tf.contrib.layers.fully_connected(output, self._config.n_tags, activation_fn=None)
        current_max_length = tf.shape(self._input_batch)[1]
        self._logits = tf.reshape(pred, [-1, current_max_length, self._config.n_tags], 'logits')

    def _pred_layer(self):
        if self._config.use_crf:
            self._trans_param = tf.get_variable('trans_param',
                                                [self._config.n_tags, self._config.n_tags])
            self._pred, _ = tf.contrib.crf.crf_decode(self._logits,
                                                      self._trans_param,
                                                      self._length_batch)
        else:
            self._pred = tf.reduce_max(self._logits, -1)
            current_max_length = tf.shape(self._input_batch)[1]
            mask = tf.sequence_mask(self._length_batch, current_max_length)
            mask_value = tf.zeros_like(self._pred)
            self._pred = tf.where(mask, self._pred, mask_value)  # 用0代替后面不预测的部分。自行根据文本长度截取预测序列

    def _loss_def(self):
        if self._config.use_crf:
            log_likelihood, _ = tf.contrib.crf.crf_log_likelihood(self._logits,
                                                                  self._target_batch,
                                                                  self._length_batch,
                                                                  self._trans_param)
            self._loss = tf.reduce_mean(-log_likelihood, name='loss')
        else:
            self._loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self._logits,
                                                                        labels=self._target_batch)
            self._loss = tf.reduce_mean(self._loss, 'loss')
        if self._config.l2_norm:
            all_vars = tf.trainable_variables()
            reg_loss = []
            for v in all_vars:
                if 'embedding' in v.name:
                    continue
                elif 'trans_param' in v.name:
                    continue
                else:
                    reg_loss.append(tf.nn.l2_loss(v))

            self._loss = self._loss + self._config.l2_norm * tf.reduce_sum(reg_loss)

    def _train_def(self):
        optimizer = tf.train.AdamOptimizer(self._learning_rate)
        self._train_op = optimizer.minimize(self._loss, global_step=self._global_step)

    def _init_session(self):
        c = tf.ConfigProto()
        c.gpu_options.allow_growth = True
        self._sess = tf.Session(config=c)
        self._sess.run(tf.global_variables_initializer())
        self._saver = tf.train.Saver(max_to_keep=self._config.save_n_times)

    def fit(self, data_set, retrain=True):
        """ 在数据集上做拟合

        :param DataSet data_set: 数据集
        :param retrain: 设置成False，则模型参数从保存的结果中恢复出来，不做tf全局初始化。意味着接着上一次训练得到的模型继续训练；
                        设置成True，意味着所有的模型参数重新初始化，重新训练
        """
        if retrain:
            self._sess.run(tf.global_variables_initializer())
        self._sess.run(self._iter.initializer, feed_dict={self._inputs: data_set.sentences,
                                                          self._lengths: data_set.lengths,
                                                          self._targets: data_set.tags})

        for i in range(1, self._config.epochs+1):
            self._sess.run(self._train_op)

            if i % self._config.output_n_epochs == 0:
                loss = self._sess.run(self._loss)
                print('epoch {}: loss is {}'.format(i, loss))

            if i % self._config.save_n_steps == 0:
                self.save(os.path.join(self._config.save_path, self._config.model_name), self._global_step)

    def predict(self, data_set):
        if self._config.rnn_drop:
            pred = self._sess.run(self._pred, feed_dict={self._input_batch: data_set.sentences,
                                                         self._length_batch: data_set.lengths,
                                                         self._drop_rate: 0})
        else:
            pred = self._sess.run(self._pred, feed_dict={self._input_batch: data_set.sentences,
                                                         self._length_batch: data_set.lengths})
        return pred

    def save(self, path, step=None):
        self._saver.save(self._sess, path, step)

    def restore(self, path=None):
        # self._saver.restore(self._sess, tf.train.latest_checkpoint(path))
        if path is None:
            self._saver.restore(self._sess,
                                os.path.join(self._config.save_path,
                                             '{}-{}'.format(self._config.model_name, self._config.epochs)))
        else:
            self._saver.restore(self._sess, path)

    def export(self, path):
        builder = tf.saved_model.builder.SavedModelBuilder(path)
        input_info = tf.saved_model.utils.build_tensor_info(self._input_batch)
        length_info = tf.saved_model.utils.build_tensor_info(self._length_batch)
        output_info = tf.saved_model.utils.build_tensor_info(self._pred)

        prediction_signature = (
            tf.saved_model.signature_def_utils.build_signature_def(
                inputs={'input': input_info,
                        'length': length_info},
                outputs={'pred': output_info},
                method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))

        builder.add_meta_graph_and_variables(self._sess,
                                             [tf.saved_model.tag_constants.SERVING],
                                             signature_def_map={
                                                 'predict': prediction_signature
                                             })
        builder.save()


class EntityDependencyRNNModel(RNNModel):
    def build(self):
        with tf.variable_scope(self._config.model_name):
            self._placeholder()
            self._build_set()
            self._embedding_layer()
            self._rnn_layer()
            self._entity_dependency_layer()
            self._pred_layer()
            self._loss_def()
            self._train_def()
            self._init_session()

    def _rnn_layer(self):
        if self._config.rnn_type == 'lstm':
            self._cell_fw = tf.contrib.rnn.BasicLSTMCell(self._config.rnn_size)
            self._cell_bw = tf.contrib.rnn.BasicLSTMCell(self._config.rnn_size)
        elif self._config.rnn_type == 'gru':
            self._cell_fw = tf.contrib.rnn.GRUCell(self._config.rnn_size)
            self._cell_bw = tf.contrib.rnn.GRUCell(self._config.rnn_size)

        self._state_fw = self._cell_fw.zero_state(tf.shape(self._input_batch)[0], tf.float32)
        self._state_bw = self._cell_bw.zero_state(tf.shape(self._input_batch)[0], tf.float32)

        self._hidden, _ = tf.nn.bidirectional_dynamic_rnn(self._cell_fw,
                                                          self._cell_bw,
                                                          self._embedding_inputs,
                                                          sequence_length=self._length_batch,
                                                          initial_state_fw=self._state_fw,
                                                          initial_state_bw=self._state_bw)

    def _entity_dependency_layer(self):
        forward_hidden, backward_hidden = self._hidden
        out_fw, out_bw = _bi_dynamic_at(forward_hidden,
                                        backward_hidden,
                                        self._config.n_step,
                                        sequence_length=self._length_batch)
        out_concat = tf.concat((out_fw, out_bw), axis=-1)

        output = tf.reshape(out_concat, [-1, 2 * self._config.rnn_size])
        pred = tf.contrib.layers.fully_connected(output, self._config.n_tags, activation_fn=None)
        current_max_length = tf.shape(self._input_batch)[1]
        self._logits = tf.reshape(pred, [-1, current_max_length, self._config.n_tags], 'logits')


def _bi_dynamic_at(input_fw,
                   input_bw,
                   n_step,
                   score_function=tf.nn.softmax,
                   time_major=False,
                   sequence_length=None):
    output_fw = _dynamic_at(input_fw, n_step, score_function, time_major, sequence_length)

    if time_major:
        time_axis = 0
        batch_axis = 1
    else:
        time_axis = 1
        batch_axis = 0

    inputs_reverse = tf.reverse_sequence(input=input_bw,
                                         seq_lengths=sequence_length,
                                         seq_axis=time_axis,
                                         batch_axis=batch_axis)
    tmp = _dynamic_at(inputs_reverse, n_step, score_function, time_major, sequence_length)
    output_bw = tf.reverse_sequence(input=tmp,
                                    seq_lengths=sequence_length,
                                    seq_axis=time_axis,
                                    batch_axis=batch_axis)
    return output_fw, output_bw


def _dynamic_at(inputs,
                n_step,
                score_function=tf.nn.softmax,
                time_major=False,
                sequence_length=None):
    """

    :param inputs: outputs of rnn
    :param n_step: 根据前n_step个hidden来计算attention score
    :param score_function: 用来计算score的函数，默认softmax
    :param time_major: if time_major, shape of inputs is [time, batch, ...], else [batch, time, ...]
    :param sequence_length: 一个batch中每个句子的长度
    :return:
    """

    if not time_major:
        # (B, T, N) -> (T, B, N)
        inputs = tf.transpose(inputs, [1, 0, 2])

    outputs = _dynamic_at_loop(inputs, n_step, score_function, sequence_length)

    if not time_major:
        outputs = tf.transpose(inputs, [1, 0, 2])

    return outputs


def _dynamic_at_loop(inputs,
                     n_step,
                     score_function=tf.nn.softmax,
                     sequence_length=None):
    input_shape = tf.shape(inputs)
    time_steps, batch_size, _ = input_shape[0], input_shape[1], input_shape[2]
    input_size = inputs.shape.as_list()[2]
    zero_out = tf.zeros((batch_size, input_size))

    with tf.name_scope("entity_dependency") as scope:
        base_name = scope

    def _create_ta(name, element_shape, dtype):
        return tf.TensorArray(dtype=dtype,
                              size=time_steps,
                              element_shape=element_shape,
                              tensor_array_name=base_name + name)

    # ta abbreviation for TensorArray
    output_ta = _create_ta('output',
                           element_shape=inputs.shape[1:],
                           dtype=inputs.dtype)
    inputs_ta = _create_ta('input',
                           element_shape=inputs.shape[1:],
                           dtype=inputs.dtype)
    inputs_ta = inputs_ta.unstack(inputs)  # unpack to array
    # inputs_ta = tf.unstack(inputs, inputs_ta)

    # create attention_weight
    attention_weight = tf.Variable(tf.random_normal([input_size, 1], stddev=tf.sqrt(2 / (input_size + 1))),
                                   dtype=tf.float32)

    def _time_step(time, output_ta_t):  # time start from 0
        if time == 0:
            output_ta_t.write(time, inputs_ta.read(time))
            return time+1, output_ta_t

        def gather1():
            return inputs_ta.gather(tf.range(0, time+1)), output_ta_t.gather(tf.range(0, time))

        def gather2():
            return inputs_ta.gather(tf.range(time-n_step+1, time+1)), output_ta_t.gather(tf.range(time-n_step+1, time))

        inputs_ts, outputs_ts = tf.cond(tf.less(time, n_step), gather1, gather2)

        def cal_out():
            at_array = tf.concat((inputs_ts, outputs_ts), 1)  # [2 * n_step-1, B, N]
            at_score = tf.reshape(at_array, [-1, input_size])  # [(2*n_step-1)*B, N]
            at_score = score_function(at_score @ attention_weight)  # [(2*n_step-1)*B, 1]

            def score1():
                return tf.reshape(at_score, [2*time-1, batch_size, 1])

            def score2():
                return tf.reshape(at_score, [2*n_step-1, batch_size, 1])

            at_score = tf.cond(tf.less(time, n_step), score1, score2)  # [2*n_step-1, batch_size, 1]

            output = tf.reduce_sum(at_array * at_score, 0)  # [B, N]
            return output

        if sequence_length is not None:
            cond = time >= sequence_length  # 哪些序列已经结束
            temp_out = cal_out()
            out = tf.where(cond, temp_out, zero_out)
        else:
            out = cal_out()

        output_ta_t.write(time, out)
        return time+1, output_ta_t

    time = tf.constant(0, dtype=tf.int32, name='time')
    _, final_out = tf.while_loop(cond=lambda time, *_: time < time_steps,
                                 body=_time_step,
                                 loop_vars=(time, output_ta),
                                 maximum_iterations=time_steps)

    return final_out.stack()
