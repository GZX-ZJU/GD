import os
import numpy as np
import pandas as pd

from .set import DataSet


tag_lists = {"first": ['O',
                       'B-ZS', 'I-ZS',  # 主诉
                       'B-ZSAC', 'I-ZSAC',
                       'B-ZSAS', 'I-ZSAS',
                       'B-ZSDC', 'I-ZSDC',
                       'B-ZSOC', 'I-ZSOC',
                       'B-ZSADV', 'I-ZSADV',
                       'B-ZSCOMA', 'I-ZSCOMA',
                       'B-GR', 'I-GR',  # 个人史
                       'B-HY', 'I-HY',
                       'B-JW', 'I-JW',  # 既往史
                       'B-JZ', 'I-JZ',  # 家族史
                       'B-XB', 'I-XB',  # 现病史
                       'B-YJ', 'I-YJ',
                       ],
             "second": ['None',
                        'B-Operation', 'M-Operation', 'E-Operation', 'S-Operation',
                        'B-Body', 'M-Body', 'E-Body', 'S-Body',
                        'B-Description', 'M-Description', 'E-Description', 'S-Description',
                        'B-Symptom', 'M-Symptom', 'E-Symptom', 'S-Symptom',
                        'B-Drug', 'M-Drug', 'E-Drug', 'S-Drug'],
             "third": ['None',
                       'B-Operation', 'I-Operation',
                       'B-Body', 'I-Body',
                       'B-Description', 'I-Description',
                       'B-Symptom', 'I-Symptom',
                       'B-Drug', 'I-Drug']
             }


def process_data_set(data_set, word2id, tag2id, max_length=None):
    """将数据集data_set中的sentences, tags转为NERModel 可接受的形式

    :param DataSet data_set:
    :param dict word2id:
    :param dict tag2id:
    :param int max_length:
    :return:
    """
    sequence = sentences_to_id(data_set.sentences, word2id, max_length)
    targets = sentences_to_id(data_set.tags, tag2id, max_length)
    return DataSet(sentences=sequence, tags=targets, lengths=data_set.lengths)


def sentences_to_id(sentences, word2id, max_length=None):
    """将文本转为id的表达。该函数同样适用于标签的转换

    :param list sentences: sentences [['今', '日', '查', '房'], ['患', '者'], ....] or ['今日查房', '患者....', ...]
    :param dict word2id: {'UNK': 0, '今': 1, '日': 2, ...}
    :param max_length: the max length of one sentence
    :return: numpy.ndarray
    """
    default_value = word2id.get('UNK', 0)
    ids, lengths = [], []
    num_sentences = len(sentences)
    for sentence in sentences:
        ids.append(list(map(lambda x: word2id.get(x, default_value), sentence)))
        lengths.append(len(sentence))

    max_length = max_length or max(lengths)
    result = np.zeros([num_sentences, max_length])

    for i, sids in enumerate(ids):
        result[i, :lengths[i]] = np.array(sids)

    return result


def ch_to_en_punctuation(sentence):
    """ trans chinese punctuation to english punctuation

    :param str sentence: origin sentence
    :return: str with english punctuation
    """
    chinese_punctuation = '，。【】“”‘’！？（）１２３４５６７８９０'
    english_punctuation = ',.[]""\'\'!?()1234567890'
    table = str.maketrans(chinese_punctuation, english_punctuation)
    return sentence.translate(table)


def full2half(sentence):
    """全角转半角

    :param sentence: 原句
    :return: 转为后续的
    """
    half = ''
    for s in sentence:
        num = ord(s)
        if num == 0x3000:
            num = 32
        elif 0xFF01 <= num <= 0xFF5E:
            num -= 0xFEE0
        u = chr(num)
        half += u
    return half


def trans_data_format():
    root_path = '../resources/data/'
    prefix = '入院记录现病史-'
    result_file = os.path.join(root_path, 'result.txt')
    if os.path.exists(result_file):
        os.remove(result_file)

    map_dict = {'解剖部位': 'Body',
                '症状描述': 'Description',
                '独立症状': 'Symptom',
                '药物': 'Drug',
                '手术': 'Operation'}

    for i in range(1, 601):
        tag_file = os.path.join(root_path, '{}{}.txt'.format(prefix, i))
        origin_file = os.path.join(root_path, '{}{}.txtoriginal.txt'.format(prefix, i))

        with open(origin_file, 'r', encoding='utf-8') as o:
            text = o.read().strip()

        with open(tag_file, 'r', encoding='utf-8') as t:
            tags = pd.read_csv(t, sep='\t', header=None)

        target = ['O' for _ in range(len(text))]
        for _, row in tags.iterrows():
            word, start_idx, stop_idx, tag = row
            if len(word) == 1:
                target[start_idx] = 'S-{}'.format(map_dict[tag])
            elif len(word) == 2:
                target[start_idx] = 'B-{}'.format(map_dict[tag])
                target[stop_idx-1] = 'E-{}'.format(map_dict[tag])
            else:
                target[start_idx] = 'B-{}'.format(map_dict[tag])
                target[stop_idx - 1] = 'E-{}'.format(map_dict[tag])
                target[start_idx + 1: stop_idx - 1] = ['M-{}'.format(map_dict[tag]) for _ in range(len(word)-2)]

        with open(result_file, 'a', encoding='utf-8') as wf:
            for c, t in zip(text, target):
                if c == '。':
                    wf.write('{}\t{}\n\n'.format(c, t))
                else:
                    wf.write('{}\t{}\n'.format(c, t))


def remove_long_sentences(file_path=None):
    if file_path is None:
        f_root = os.path.dirname(__file__)
        file_path = os.path.join(f_root, '../resources/corpus/result.txt')

    sentences, targets, lengths = [], [], []
    with open(file_path, 'r', encoding='utf-8') as df:
        sentence, target = [], []
        for line in df:
            if line == '\n':
                sentences.append(sentence)
                targets.append(target)
                lengths.append(len(sentence))
                sentence, target = [], []
            else:
                word, tag = line.split('\t')
                sentence.append(word)
                target.append(tag.strip())

    sentences = np.array(sentences)
    targets = np.array(targets)
    lengths = np.array(lengths)
    sentences = sentences[lengths < 200]
    targets = targets[lengths < 200]

    with open(file_path, 'w', encoding='utf-8') as df:
        for sentence, target in zip(sentences, targets):
            for s, t in zip(sentence, target):
                df.write('{}\t{}\n'.format(s, t))
            df.write('\n')


def split_data_set(file_path=None, train=0.7, val=0.1):
    """ give train and validation ratio, test ratio = 1 - train - validation

    :param file_path: file to data file
    :param train: ratio of train set
    :param val: ratio of validation set
    :return:
    """
    assert 0 < train < 1
    assert 0 < val < 1
    assert 0 < train + val < 1

    if file_path is None:
        f_root = os.path.dirname(__file__)
        file_path = os.path.join(f_root, '../resources/corpus/result.txt')

    sentences, targets, lengths = [], [], []
    with open(file_path, 'r', encoding='utf-8') as df:
        sentence, target = [], []
        for line in df:
            if line == '\n':
                sentences.append(sentence)
                targets.append(target)
                lengths.append(len(sentence))
                sentence, target = [], []
            else:
                word, tag = line.split('\t')
                sentence.append(word)
                target.append(tag.strip())

    sentences = np.array(sentences)
    targets = np.array(targets)

    num = sentences.shape[0]
    train_num = int(num * train)
    val_num = int(num * val)
    indexes = np.arange(num)
    np.random.shuffle(indexes)
    train_idx = indexes[:train_num]
    val_idx = indexes[train_num: train_num+val_num]
    test_idx = indexes[train_num+val_num:]

    train_s = sentences[train_idx]
    train_t = targets[train_idx]

    val_s = sentences[val_idx]
    val_t = targets[val_idx]

    test_s = sentences[test_idx]
    test_t = targets[test_idx]

    f_root = os.path.dirname(__file__)
    train_f = os.path.join(f_root, "../resources/eval_corpus/train")
    val_f = os.path.join(f_root, "../resources/eval_corpus/val")
    test_f = os.path.join(f_root, "../resources/eval_corpus/test")

    with open(train_f, 'w', encoding='utf-8') as df:
        for sentence, target in zip(train_s, train_t):
            for s, t in zip(sentence, target):
                df.write('{}\t{}\n'.format(s, t))
            df.write('\n')

    with open(val_f, 'w', encoding='utf-8') as df:
        for sentence, target in zip(val_s, val_t):
            for s, t in zip(sentence, target):
                df.write('{}\t{}\n'.format(s, t))
            df.write('\n')

    with open(test_f, 'w', encoding='utf-8') as df:
        for sentence, target in zip(test_s, test_t):
            for s, t in zip(sentence, target):
                df.write('{}\t{}\n'.format(s, t))
            df.write('\n')
