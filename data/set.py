import os
import collections


class DataSet(object):
    """DataSet contains origin sentences, lengths and tags of sentences

    """
    def __init__(self, sentences, lengths, tags):
        self._sentences = sentences
        self._lengths = lengths
        self._tags = tags

    @property
    def sentences(self):
        return self._sentences

    @property
    def lengths(self):
        return self._lengths

    @property
    def tags(self):
        return self._tags

    @classmethod
    def ner_data_set(cls, path=None):
        if path is None:
            base_path = os.path.dirname(__file__)
            path = os.path.join(base_path, '../resources/corpus/train_data')
        return _read_data(path)


def _read_data(file_path, data_format=None):
    """read data

    :param file_path: path of data file
    :param data_format: format of saved data. supported format: 'character', 'sentence'
    :return: DataSet
    """
    data_format = data_format or 'character'

    if data_format not in ['character', 'sentence']:
        raise ValueError('Not supported data format: {}'.format(data_format))

    sentences, lengths, tags = [], [], []
    if data_format == 'character':
        with open(file_path, 'r', encoding='utf-8') as df:
            sentence, tag = [], []
            for line in df:
                if line == '\n':
                    sentences.append(sentence)
                    lengths.append(len(sentence))
                    tags.append(tag)
                    sentence, tag = [], []
                else:
                    character, t = line.split('\t')
                    sentence.append(character)
                    tag.append(t.strip())
    else:
        raise NotImplementedError

    return DataSet(sentences, lengths, tags)


def build_vocab(sentences):
    words = [w for s in sentences for w in s]
    word_list = [['UNK', -1]]
    word_list.extend(collections.Counter(words))
    word2id = {}

    for w in word_list:
        word2id[w[0]] = len(word2id)

    return word2id
