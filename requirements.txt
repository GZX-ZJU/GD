gensim==3.5.0
tensorflow==1.8.0
numpy==1.15.0
pandas==0.23.3
matplotlib==2.2.3
