import unittest

from experiments import *


class TestEvaluation(unittest.TestCase):
    gold = [{'word': '胃', 'start': 7, 'stop': 8, 'tag': 'Body'},
            {'word': '胃癌根治术', 'start': 29, 'stop': 34, 'tag': 'Operation'},
            {'word': '腹腔', 'start': 40, 'stop': 42, 'tag': 'Body'},
            {'word': '发热', 'start': 435, 'stop': 437, 'tag': 'Symptom'}]
    prediction = [{'word': '胃', 'start': 7, 'stop': 8, 'tag': 'Body'},
                  {'word': '腹腔', 'start': 40, 'stop': 42, 'tag': 'Body'},
                  {'word': '腹腔', 'start': 44, 'stop': 46, 'tag': 'Body'},
                  {'word': '淋巴结', 'start': 253, 'stop': 256, 'tag': 'Body'},
                  {'word': '发热', 'start': 435, 'stop': 437, 'tag': 'Symptom'}]

    def test_precision(self):
        actual = precision(TestEvaluation.prediction, TestEvaluation.gold)
        expected = 3 / 5
        self.assertEqual(actual, expected)

    def test_recall(self):
        actual = recall(TestEvaluation.prediction, TestEvaluation.gold)
        expected = 3 / 4
        self.assertEqual(actual, expected)

    def test_f1_score(self):
        actual = f1_score(TestEvaluation.prediction, TestEvaluation.gold)
        expected = 2 * (3/5 * 3/4) / (3/5 + 3/4)
        self.assertEqual(actual, expected)

    def test_evaluate_all_case1(self):
        prediction = [TestEvaluation.prediction, TestEvaluation.prediction]
        gold = [TestEvaluation.gold, TestEvaluation.gold]
        p, r, f = evaluate_all(prediction, gold, mode='all')
        p_expected = [3 / 5, 3 / 5]
        r_expected = [3 / 4, 3 / 4]
        f_expected = [2 * (3/5 * 3/4) / (3/5 + 3/4), 2 * (3/5 * 3/4) / (3/5 + 3/4)]
        self.assertEqual(p, p_expected)
        self.assertEqual(r, r_expected)
        self.assertEqual(f, f_expected)

    def test_evaluate_all_case2(self):
        prediction = [TestEvaluation.prediction, TestEvaluation.prediction]
        gold = [TestEvaluation.gold, TestEvaluation.gold]
        p, r, f = evaluate_all(prediction, gold, mode='mean')
        p_expected = 3 / 5
        r_expected = 3 / 4
        f_expected = 2 * (3/5 * 3/4) / (3/5 + 3/4)
        self.assertEqual(p, p_expected)
        self.assertEqual(r, r_expected)
        self.assertEqual(f, f_expected)


class TestExperiment(unittest.TestCase):
    def test_save_and_load_config(self):
        config = ModelConfig()
        config_file = '../configs/test_config_load/config.json'
        save_config(config, config_file)
        actual = load_model_config(config_file)
        expected = ModelConfig()
        self.assertTrue(actual == expected)

    def test_load_config1(self):
        actual = load_model_config('../configs/test_config_load/config1.json')
        expected = ModelConfig()
        expected.learning_rate = 0.002
        self.assertTrue(actual == expected)


if __name__ == "__main__":
    unittest.main()
