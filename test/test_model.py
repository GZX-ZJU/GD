import unittest

from models import *


class TestConfig(unittest.TestCase):
    def test_w2v_config_equal(self):
        config1 = W2VConfig()
        config2 = W2VConfig()
        self.assertTrue(config1 == config2)

    def test_w2v_config_unequal_case1(self):
        config1 = W2VConfig()
        config2 = W2VConfig()
        config2.vocab_size += 1
        self.assertFalse(config1 == config2)

    def test_w2v_config_unequal_case2(self):
        config1 = W2VConfig()
        config2 = W2VConfig()
        config2.a = 'a'
        self.assertFalse(config1 == config2)

    def test_model_config_equal(self):
        config1 = ModelConfig()
        config2 = ModelConfig()
        self.assertTrue(config1 == config2)

    def test_model_config_unequal(self):
        config1 = ModelConfig()
        config2 = ModelConfig()
        config2.learning_rate += 0.001
        self.assertFalse(config1 == config2)

    def test_model_config_unequal(self):
        config1 = ModelConfig()
        config2 = ModelConfig()
        config2.a = 'a'
        self.assertFalse(config1 == config2)


if __name__ == "__main__":
    unittest.main()
