import unittest

from test.test_data import *
from test.test_experiment import *
from test.test_model import *


if __name__ == "__main__":
    unittest.main()
