import unittest

from data import *


class TestPreProcess(unittest.TestCase):
    word2id = {'UNK': 0, '今': 1, '日': 2, '查': 3, '房': 4, '患': 5, '者': 6,
               '无': 7, '不': 8, '适': 9, '未': 10, '闻': 11, '及': 12, '干': 13,
               '湿': 14, '性': 15, '罗': 16, '音': 17}

    def test_sentences_to_id_case1(self):
        sentences = ['今日查房', '患者无不适', '未闻及干湿性罗音']
        gold = np.array([[1, 2, 3, 4, 0, 0, 0, 0],
                         [5, 6, 7, 8, 9, 0, 0, 0],
                         [10, 11, 12, 13, 14, 15, 16, 17]])

        result = sentences_to_id(sentences, TestPreProcess.word2id)
        self.assertTrue((result == gold).all())

    def test_sentences_to_id_case2(self):
        sentences = [['今', '日', '查', '房'],
                     ['患', '者', '无', '不', '适'],
                     ['未', '闻', '及', '干', '湿', '性', '罗', '音']]
        gold = np.array([[1, 2, 3, 4, 0, 0, 0, 0],
                         [5, 6, 7, 8, 9, 0, 0, 0],
                         [10, 11, 12, 13, 14, 15, 16, 17]])

        result = sentences_to_id(sentences, TestPreProcess.word2id)
        self.assertTrue((result == gold).all())

    def test_ch_to_en_punctuation(self):
        sentence = "中国，中文。标点符号！‘’“”"
        gold = "中国,中文.标点符号!''\"\""

        result = ch_to_en_punctuation(sentence)
        self.assertEqual(result, gold)

    def test_full_to_half(self):
        sentence = 'ａｂｃｄ１２３４．'
        gold = 'abcd1234.'

        result = full2half(sentence)
        self.assertEqual(result, gold)


class TestPostProcess(unittest.TestCase):
    def test_ids_to_tag_case1(self):
        id2tag = {0: 'B', 1: 'I', 2: 'O'}
        ids = [[0, 1, 2, 0, 1],
               [1, 2, 0, 1, 2]]

        gold = [['B', 'I', 'O', 'B', 'I'],
                ['I', 'O', 'B', 'I', 'O']]
        result = ids_to_tags(ids, id2tag)
        self.assertEqual(result, gold)

    def test_ids_to_tag_case2(self):
        id2tag = {0: 'B', 1: 'I', 2: 'O'}
        ids = [[0, 1, 2, 0, 0],
               [1, 2, 0, 1, 2]]

        gold = [['B', 'I', 'O', 'B'],
                ['I', 'O', 'B', 'I', 'O']]
        lengths = [4, 5]
        result = ids_to_tags(ids, id2tag, lengths)
        self.assertEqual(result, gold)

    def test_split_by_tags_case1(self):
        sentences = ['今日查房', '患者无不适', '未闻及干湿性罗音']
        tags = [['B-Time', 'I-Time', 'O', 'O'],
                ['O', 'O', 'O', 'B-Symptom', 'I-Symptom'],
                ['O', 'O', 'O', 'B-Symptom', 'I-Symptom', 'I-Symptom', 'I-Symptom', 'I-Symptom']]

        gold = [[{'word': '今日', 'start': 0, 'end': 2, 'tag': 'Time'}],
                [{'word': '不适', 'start': 3, 'end': 5, 'tag': 'Symptom'}],
                [{'word': '干湿性罗音', 'start': 3, 'end': 8, 'tag': 'Symptom'}]]
        result = split_by_tags(sentences, tags)
        self.assertEqual(result, gold)

    def test_split_by_tags_case2(self):
        sentences = [['今', '日', '查', '房'],
                     ['患', '者', '无', '不', '适'],
                     ['未', '闻', '及', '干', '湿', '性', '罗', '音']]
        tags = [['B-Time', 'I-Time', 'O', 'O'],
                ['O', 'O', 'O', 'B-Symptom', 'I-Symptom'],
                ['O', 'O', 'O', 'B-Symptom', 'I-Symptom', 'I-Symptom', 'I-Symptom', 'I-Symptom']]

        gold = [[{'word': '今日', 'start': 0, 'end': 2, 'tag': 'Time'}],
                [{'word': '不适', 'start': 3, 'end': 5, 'tag': 'Symptom'}],
                [{'word': '干湿性罗音', 'start': 3, 'end': 8, 'tag': 'Symptom'}]]
        result = split_by_tags(sentences, tags)
        self.assertEqual(result, gold)


class TestNERDataSetLoad(unittest.TestCase):
    def test_ner_train_data_set(self):
        data_set = DataSet.ner_data_set()
        expected = list('主诉：确诊左肺腺癌伴全身多发转移6月余。')
        actual = data_set.sentences[0]
        self.assertEqual(actual, expected)

        expected = list('主诉：经期腹痛且月经量明显增多2年。')
        actual = data_set.sentences[-1]
        self.assertEqual(actual, expected)

    def test_ner_test_data_set(self):
        data_set = DataSet.ner_data_set('../resources/corpus/test_data')
        expected = list('主诉：反复右上腹胀满不适一年余。')
        actual = data_set.sentences[0]
        self.assertEqual(actual, expected)

        expected = list('主诉：右手不自主的抽搐4月余。')
        actual = data_set.sentences[-1]
        self.assertEqual(actual, expected)


if __name__ == '__main__':
    unittest.main()
