import argparse
import experiments


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--train", dest='train', action='store_true')
    parser.add_argument("--test", dest='train', action='store_false')
    parser.set_defaults(train=True)

    parser.add_argument("--compare", dest='compare', action='store_true')
    parser.set_defaults(compare=False)

    parser.add_argument("--config_file", type=str,
                        help='config file of model')
    parser.add_argument("--config_file2", type=str,
                        help='config file of model')
    parser.add_argument("--checkpoint_path1", type=str)
    parser.add_argument("--checkpoint_path2", type=str)

    parser.add_argument("--train_file", type=str,
                        help='train data file')
    parser.add_argument("--test_file", type=str,
                        help='test data file')

    parser.add_argument('--tag_list', type=str, default='first',
                        choices=['first', 'second', 'third'])
    parser.add_argument('--tag_format', type=str, default='BIO',
                        choices=['BIO', 'BMESO'])

    args = parser.parse_args()
    proceed(args)


def proceed(args):
    config_file = args.config_file
    if args.train_file is not None and args.test_file is None:
        args.test_file = args.train_file

    if args.train:
        experiments.validation(model_config_file=config_file,
                               train_file=args.train_file,
                               test_file=args.test_file,
                               tag_list=args.tag_list,
                               tag_format=args.tag_format)
    else:
        experiments.test(config_file=config_file,
                         test_file=args.test_file,
                         tag_list=args.tag_list,
                         tag_format=args.tag_format,
                         compare=args.compare,
                         config_file2=args.config_file2,
                         checkpoint_path1=args.checkpoint_path1,
                         checkpoint_path2=args.checkpoint_path2)


if __name__ == "__main__":
    main()
