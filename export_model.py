import os
import argparse

import tensorflow as tf

from models import make_model, load_model_config


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--config_file", type=str,
                        help='config file of model')
    parser.add_argument("--checkpoint_path", type=str)

    parser.add_argument("--export_path", type=str, default='./exported_model')
    parser.add_argument("--export_version", type=int, default='1')

    args = parser.parse_args()
    proceed(args)


def proceed(args):
    config_file = args.config_file
    config = load_model_config(config_file)
    if args.checkpoint_path is None:
        checkpoint_path = tf.train.latest_checkpoint(config.save_path)
    else:
        checkpoint_path = os.path.join(config.save_path, args.checkpoint_path)
    model = make_model(config)
    model.restore(checkpoint_path)
    export_path = os.path.join(tf.compat.as_bytes(args.export_path),
                               tf.compat.as_bytes(str(args.export_version)))
    model.export(export_path)


if __name__ == "__main__":
    main()
